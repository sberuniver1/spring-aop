package ru.edu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.edu.service.UserCache;
import ru.edu.service.UserInfo;

import java.util.Collection;
import java.util.Objects;

@RestController
@RequestMapping(value = "/api/user", consumes = MediaType.ALL_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserRestController {
    private UserCache userCache;

    @GetMapping("/all")
    public Collection<UserInfo> getAll() {
        return userCache.getAll();
    }

    @GetMapping
    public UserInfo getUser(@RequestParam("userId") String id) {
        return userCache.getById(id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserInfo createUser(@RequestBody UserInfo info) {
        return userCache.create(info);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserInfo updateUser(@RequestBody UserInfo info) {
        return userCache.update(info);
    }

    @DeleteMapping("/{userId}")
    public UserInfo remove(@PathVariable("userId") String id) {
        UserInfo userInfo = userCache.getById(id);
        if (Objects.isNull(userInfo)) {
            throw new IllegalArgumentException("User id =" + id + " not found");
        }
        return userCache.remote(id);
    }

    @Autowired
    public void setUserCache(UserCache userCache) {
        this.userCache = userCache;
    }

}
