package ru.edu.logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Aspect
@Component
public class LoggerAspect {

    @Pointcut("execution(* ru.edu.UserRestController.*(..))")
    public void userRestControllerPointcut() {
    }

    @Before("userRestControllerPointcut()")
    public void beforeMethod(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);
        System.out.println("Method=" + methodName + " args=" + args + " started");

    }

    @AfterReturning("userRestControllerPointcut()")
    public void afterMethod(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);
        System.out.println("Method=" + methodName + " args=" + args + " finished");
    }

    @AfterThrowing(value = "userRestControllerPointcut()", throwing = "ex")
    public void afterException(JoinPoint joinPoint, Throwable ex) {
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);
        String message = String.format("ERROR!!! Failed to method=%s args=%s error=%s", methodName, args, ex);
        System.out.println(message);
    }

    private List<String> getArgs(JoinPoint joinPoint) {
        List<String> args = new ArrayList<>();
        for (int i = 0; i < joinPoint.getArgs().length; ++i) {
            Object argValue = joinPoint.getArgs()[i];
            args.add("arg." + i + "=" + argValue);
        }
        return args;
    }
}
