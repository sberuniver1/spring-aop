package ru.edu.service;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Component
public class UserCache {
    private final Map<String, UserInfo> users = new HashMap<>();

    @PostConstruct
    public void init() {

        create(new UserInfo("1", "Anton"));
        create(new UserInfo("2", "Denis"));
        create(new UserInfo("3", "Vasiliy"));
    }

    public UserInfo create(UserInfo info) {
        return users.put(info.getId(), info);
    }

    public UserInfo getById(String id) {
        return users.get(id);
    }

    public UserInfo update(UserInfo info) {
        return users.put(info.getId(), info);
    }

    public UserInfo remote(String id) {
        return users.remove(id);
    }

    public Collection<UserInfo> getAll() {
        return users.values();
    }


}
