package ru.edu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.service.UserCache;
import ru.edu.service.UserInfo;

import java.util.Collection;

@Controller
@RequestMapping(value = "/user", consumes = MediaType.ALL_VALUE)
public class UserViewController {
    private UserCache userCache;
    private String authorName;

    @Value("${program.author.name}")
    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }


    @Autowired
    public void setUserCache(UserCache userCache) {
        this.userCache = userCache;
    }

    @GetMapping
    public ModelAndView info() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/hello.jsp");
        modelAndView.addObject("author", authorName);
        return modelAndView;
    }

    @GetMapping("/all")
    public ModelAndView getAllUsers() {
        Collection<UserInfo> users = userCache.getAll();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/all_users.jsp");
        modelAndView.addObject("userList", users);
        return modelAndView;
    }
}
